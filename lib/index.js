var fs = require('fs'),
    path = require('path');

var modelsDirectory = process.cwd() + '/app/models';

module.exports = {
    configure: function(directory) {
        modelsDirectory = process.cwd() + '/' + directory;
    },
    load: function(modelName) {
        return new require(modelsDirectory + '/' + modelName);
    }
};
